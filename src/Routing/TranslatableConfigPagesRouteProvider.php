<?php

namespace Drupal\translatable_config_pages\Routing;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\Routing\AdminHtmlRouteProvider;
use Drupal\translatable_config_pages\Entity\Controller\TranslatableConfigPagesEntityController;
use Symfony\Component\Routing\Route;

/**
 * Provides HTML routes for entities with administrative add pages.
 */
class TranslatableConfigPagesRouteProvider extends AdminHtmlRouteProvider {

  /**
   * {@inheritdoc}
   */
  protected function getAddPageRoute(EntityTypeInterface $entity_type) {
    if ($entity_type->hasLinkTemplate('add-page') && $entity_type->getKey('bundle')) {
      $route = new Route($entity_type->getLinkTemplate('add-page'));
      $route->setDefault('_controller', TranslatableConfigPagesEntityController::class . '::addPage');
      $route->setDefault('_title_callback', TranslatableConfigPagesEntityController::class . '::addTitle');
      $route->setDefault('entity_type_id', $entity_type->id());
      $route->setRequirement('_entity_create_any_access', $entity_type->id());

      return $route;
    }
  }

}
