<?php

namespace Drupal\translatable_config_pages;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Defines the access control handler for the translatable.
 *
 * Config pages entity type.
 */
class TranslatableConfigPagesAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    return match ($operation) {
      // Operation view: Necessary to add the translation.
      'view' => AccessResult::allowedIfHasPermission($account, 'view translatable config pages'),
      'update' => AccessResult::allowedIfHasPermissions(
        $account,
        [
          'manage translatable config pages',
          'administer translatable config pages',
        ],
        'OR',
      ),
      default => AccessResult::neutral(),
    };
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermissions(
      $account,
      [
        'manage translatable config pages',
        'administer translatable config pages',
      ],
      'OR',
    );
  }

}
