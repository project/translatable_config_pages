<?php

namespace Drupal\translatable_config_pages\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\translatable_config_pages\TranslatableConfigPagesInterface;

/**
 * Defines the translatable config pages entity class.
 *
 * @ContentEntityType(
 *   id = "translatable_config_pages",
 *   label = @Translation("Translatable config pages"),
 *   label_collection = @Translation("Translatable config pages"),
 *   label_singular = @Translation("translatable config pages"),
 *   label_plural = @Translation("Translatable config pages"),
 *   label_count = @PluralTranslation(
 *     singular = "@count translatable config pages",
 *     plural = "@count translatable config pages",
 *   ),
 *   bundle_label = @Translation("Translatable config pages type"),
 *   handlers = {
 *     "list_builder" = "Drupal\translatable_config_pages\TranslatableConfigPagesListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "access" = "Drupal\translatable_config_pages\TranslatableConfigPagesAccessControlHandler",
 *     "form" = {
 *       "default" = "Drupal\translatable_config_pages\Form\TranslatableConfigPagesForm",
 *       "add" = "Drupal\translatable_config_pages\Form\TranslatableConfigPagesForm",
 *       "edit" = "Drupal\translatable_config_pages\Form\TranslatableConfigPagesForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\translatable_config_pages\Routing\TranslatableConfigPagesRouteProvider",
 *     }
 *   },
 *   base_table = "translatable_config_pages",
 *   data_table = "translatable_config_pages_field_data",
 *   translatable = TRUE,
 *   admin_permission = "administer translatable config pages types",
 *   entity_keys = {
 *     "id" = "id",
 *     "langcode" = "langcode",
 *     "bundle" = "bundle",
 *     "uuid" = "uuid",
 *   },
 *   links = {
 *     "collection" = "/admin/config/system/translatable-config-pages",
 *     "add-form" = "/admin/config/translatable-config-pages/add/{translatable_config_pages_type}",
 *     "add-page" = "/admin/config/translatable-config-pages/add",
 *     "canonical" = "/admin/config/translatable-config-pages/{translatable_config_pages}",
 *     "edit-form" = "/admin/config/translatable-config-pages/{translatable_config_pages}/edit",
 *   },
 *   bundle_entity_type = "translatable_config_pages_type",
 *   field_ui_base_route = "entity.translatable_config_pages_type.edit_form",
 * )
 */
class TranslatableConfigPages extends ContentEntityBase implements TranslatableConfigPagesInterface {

  /**
   * {@inheritdoc}
   */
  public function label() {
    if ($this->getEntityType()?->getKey('bundle')) {
      $bundle = $this->getEntityKey('bundle');
      $types = TranslatableConfigPagesType::loadMultiple();
      $bundleDefinition = $types[$bundle] ?? NULL;
      return $bundleDefinition?->label() ?? '';
    }
    return '';
  }

}
