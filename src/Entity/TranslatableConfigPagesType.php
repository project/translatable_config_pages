<?php

namespace Drupal\translatable_config_pages\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the Translatable config pages type entity.
 *
 * @ConfigEntityType(
 *   id = "translatable_config_pages_type",
 *   label = @Translation("Translatable config pages type"),
 *   label_collection = @Translation("Translatable config pages types"),
 *   label_singular = @Translation("Translatable config pages type"),
 *   label_plural = @Translation("Translatable config pages types"),
 *   label_count = @PluralTranslation(
 *     singular = "@count translatable config pages type",
 *     plural = "@count translatable config pages types",
 *   ),
 *   handlers = {
 *     "form" = {
 *       "add" = "Drupal\translatable_config_pages\Form\TranslatableConfigPagesTypeForm",
 *       "edit" = "Drupal\translatable_config_pages\Form\TranslatableConfigPagesTypeForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm",
 *     },
 *     "list_builder" = "Drupal\translatable_config_pages\TranslatableConfigPagesTypeListBuilder",
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     }
 *   },
 *   admin_permission = "administer translatable config pages types",
 *   bundle_of = "translatable_config_pages",
 *   config_prefix = "translatable_config_pages_type",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid",
 *     "menu" = "menu",
 *   },
 *   links = {
 *     "add-form" = "/admin/structure/translatable_config_pages_types/add",
 *     "edit-form" = "/admin/structure/translatable_config_pages_types/manage/{translatable_config_pages_type}",
 *     "delete-form" = "/admin/structure/translatable_config_pages_types/manage/{translatable_config_pages_type}/delete",
 *     "collection" = "/admin/structure/translatable_config_pages_types"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "uuid",
 *     "menu",
 *   }
 * )
 */
class TranslatableConfigPagesType extends ConfigEntityBundleBase {

  /**
   * The machine name of this translatable config pages type.
   *
   * @var string
   */
  protected $id;

  /**
   * The human-readable name of the translatable config pages type.
   *
   * @var string
   */
  protected $label;

}
