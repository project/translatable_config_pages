<?php

namespace Drupal\translatable_config_pages;

use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Provides an interface defining an translatable config pages entity type.
 */
interface TranslatableConfigPagesInterface extends ContentEntityInterface {

}
