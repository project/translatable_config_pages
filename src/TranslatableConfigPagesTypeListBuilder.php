<?php

namespace Drupal\translatable_config_pages;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Url;

/**
 * Defines a class to build a listing of translatable.
 *
 * Config pages type entities.
 *
 * @see \Drupal\translatable_config_pages\Entity\TranslatableConfigPagesType
 */
class TranslatableConfigPagesTypeListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['title'] = $this->t('Label');
    $header['bundle'] = $this->t('Bundle');

    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $row['title'] = [
      'data' => $entity->label(),
      'class' => ['menu-label'],
    ];

    $row['bundle'] = [
      'data' => $entity->getOriginalId(),
    ];

    return $row + parent::buildRow($entity);
  }

  /**
   * {@inheritdoc}
   */
  public function render() {
    $build = parent::render();

    $build['table']['#empty'] = $this->t(
      'No translatable config pages types available. <a href=":link">Add translatable config pages type</a>.',
      [':link' => Url::fromRoute('entity.translatable_config_pages_type.add_form')->toString()]
    );

    return $build;
  }

}
