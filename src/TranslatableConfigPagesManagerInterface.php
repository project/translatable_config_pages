<?php

namespace Drupal\translatable_config_pages;

use Drupal\Core\Language\LanguageInterface;
use Drupal\translatable_config_pages\Entity\TranslatableConfigPages;

/**
 * The load service interface.
 *
 * @package Drupal\translatable_config_pages
 */
interface TranslatableConfigPagesManagerInterface {

  /**
   * Loads config page entity by bundle and language.
   *
   * @param string $bundle
   *   Config page bundle to load.
   * @param \Drupal\Core\Language\LanguageInterface|null $language
   *   The language to load.
   *
   * @return null|\Drupal\translatable_config_pages\Entity\TranslatableConfigPages
   *   Loaded config page.
   */
  public function loadConfig(string $bundle, LanguageInterface $language = NULL): ?TranslatableConfigPages;

  /**
   * Loads config page field value by bundle and language.
   *
   * @param string $bundle
   *   Config page bundle to load.
   * @param string $field
   *   The field to load.
   * @param \Drupal\Core\Language\LanguageInterface|null $language
   *   The language to load.
   *
   * @return mixed
   *   Loaded field value.
   */
  public function loadConfigFieldValue(string $bundle, string $field, LanguageInterface $language = NULL): mixed;

}
